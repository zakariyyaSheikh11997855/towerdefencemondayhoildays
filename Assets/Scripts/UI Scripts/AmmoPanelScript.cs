﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoPanelScript : MonoBehaviour {

	private Text ammoText;

	// Use this for initialization
	void Start () {
		ammoText = this.GetComponent<Text> ();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void UpdateAmmoText(int amount, int extra){
		ammoText.text = amount + " / " + extra;
	}

	public void UpdateAmmoText(string text){
		ammoText.text = text;
	}
}
