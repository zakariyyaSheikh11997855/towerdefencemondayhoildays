﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Text))]
public class PlayerUI : MonoBehaviour {
    [SerializeField]
    RectTransform playerHealthBarFill;
    [SerializeField]
    RectTransform baseHealthBarFill;
    [SerializeField]
	public Text txtFunds;
    [SerializeField]
    public Text txtAmmo;
    [SerializeField]
    Text txtPhase;
    [SerializeField]
    Text txtGameOver;
    [SerializeField]
    Text txtTowerType;
    [SerializeField]
    Text txtTowerPrice;

    bool showShop;
	bool showTower;

    public GameObject shopUI;
    public GameObject ammoUI;
    public GameObject gameOverUI;
	public GameObject towerUI;

	public RawImage damage;
	public RawImage playerDeath;

    Base b;
    GridInteraction gi;
    private GameObject player;
    private Player parentPlayer;
	private WeaponManger weaponmanger; 

    public void SetPlayer(GameObject value) {
        gi = value.GetComponent<GridInteraction>();
        player = value;
        parentPlayer = value.GetComponent<Player>();
        showShop = false;

		weaponmanger = parentPlayer.GetComponent<WeaponManger> ();
    }

    void Awake() {
        b = GameObject.FindGameObjectWithTag("Base").GetComponent<Base>();

		damage.color = new Color (damage.color.r, damage.color.g, damage.color.g, 0);

		//PLAYER DEATH SCREEN - NEED TO WORK OUT HOW TO CROSSFADE
		playerDeath.color = new Color (playerDeath.color.r, playerDeath.color.g, playerDeath.color.g, 0);
    }

    void Update() {
        if (GameManager.state != GameManager.GameState.LOST) {
			print ("is player ui plar player?" + parentPlayer); 
            SetBaseHealthBar(b.currentHealth / b.maxHealth);
            SetPhaseTxt(GameManager.state, (int)GameManager.nextPhaseCountdown);
            SetPlayerFundsTxt(player.GetComponent<Player>().funds);
            SetPlayerHealthBar(parentPlayer.currentHealth / parentPlayer.maxHealth);
            if (gi != null) {
                Tower tower = gi.currentTower;
                if (tower != null) {
                    SetTowerTxt(tower.price, tower.type);
                }
            }

			//Update Damage redness to player health
			damage.color = new Color (damage.color.r, damage.color.g, damage.color.g, (player.GetComponent<Player>().maxHealth - player.GetComponent<Player>().currentHealth)/100);


            //Show the Shop UI and TowerUI.
            if (GameManager.state == GameManager.GameState.PHASE_BUILDING) {

				if (player.GetComponent<GridInteraction> ().gridInteractionEnabled) {
					showTower = true;
				} else {
					showTower = false;
				}


                if (Input.GetKeyDown(KeyCode.Tab)) {
                    showShop = !showShop;
                }
            }

            //Turn off shop if fighting
            if (GameManager.state == GameManager.GameState.PHASE_FIGHTING) {
                showShop = false;
            }

			if (showTower) {
				towerUI.SetActive(true);
			} else {
				towerUI.SetActive(false);
			}


            if (showShop) {
                shopUI.SetActive(true);
                player.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(false);
            } else {
                shopUI.SetActive(false);
                player.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(true);
            }


			print ("This is true or false always" + weaponmanger.isActive);//((WeaponManger)FindObjectOfType (typeof(WeaponManger))).isActive);


			if (weaponmanger.isActive)//((WeaponManger)FindObjectOfType(typeof(WeaponManger))).isActive) {
			{
				ammoUI.SetActive(true);
            } else {
                ammoUI.SetActive(false);
            }
        } else {
            if (shopUI.activeInHierarchy) {
                shopUI.SetActive(false);
            }

            gameOverUI.SetActive(true);
            player.GetComponent<RigidbodyFirstPersonController>().mouseLook.SetCursorLock(false);
            GameObject ch = GameObject.FindGameObjectWithTag("Crosshair");
            if (ch.activeInHierarchy) {
                ch.SetActive(false);
            }
        }
    }

    private void SetPlayerFundsTxt(int _funds) {
        txtFunds.text = _funds.ToString();
    }

    public void SetPlayerHealthBar(float _amount) {
        playerHealthBarFill.localScale = new Vector3(_amount, 1f, 1f);

    }

    private void SetBaseHealthBar(float _amount) {

        baseHealthBarFill.localScale = new Vector3(_amount, 1f, 1f);
    }

    private void SetPhaseTxt(GameManager.GameState _state, int _countdown) {
        if (_state == GameManager.GameState.PHASE_BUILDING) {
            txtPhase.text = "Building: " + _countdown + "s";
        } else if (_state == GameManager.GameState.PHASE_FIGHTING) {
            txtPhase.text = "Battle!";
        }
    }

    private void SetTowerTxt(int _price, string _type) {
        txtTowerPrice.text = "- $" + _price;
        txtTowerType.text = _type;
    }
}
