﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour {

	GameObject player;
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col){

		if (col.tag == "Player") {
			player = col.gameObject;

			//IF the ammo is used destory it 
			//TODO network call to destory all ammo across network? 
			if (player.GetComponent<WeaponManger> ().addAmmo (20)) {
				this.gameObject.SetActive (false);
			}
			//Destroy (this.gameObject);

		}

	}
}
