﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyingEnemy : Enemy {

	public Rigidbody body;
	public ParticleSystem smoke;

	float originalHeight;
	// Use this for initialization
	public override void Start () {
		type = "Flying";
		fundsWorth = 100;
		currentHealth = maxHealth;
		originalHeight = this.transform.position.y;

		base.Start ();
	}
	
	// Update is called once per frame
	protected override void Update () {

		transform.position = new Vector3 (transform.position.x, originalHeight, transform.position.z);

        base.Update();
        //if (!smoke.gameObject.activeSelf) return;
        if (currentHealth <= maxHealth / 2) {
            //if (!smoke.isPlaying) {
			if(!smoke.gameObject.activeSelf) smoke.gameObject.SetActive(true);//smoke.Play();
            //}
        } else {
            smoke.Stop();
        }
    }

	
}
